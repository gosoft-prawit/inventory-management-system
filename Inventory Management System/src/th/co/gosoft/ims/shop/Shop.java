package th.co.gosoft.ims.shop;

import java.util.ArrayList;

import th.co.gosoft.ims.inventory.Computer;
import th.co.gosoft.ims.report.Report;
import th.co.gosoft.ims.user.Manager;
import th.co.gosoft.ims.user.Staff;

public class Shop {

	private String name;
	private String address;
	private ArrayList<Manager> managers = new ArrayList<Manager>();
	private ArrayList<Staff> staffs = new ArrayList<Staff>();
	private ArrayList<Computer> computers = new ArrayList<Computer>();
	private Report totalReport = new Report();

//	public Shop(String name, String address, ArrayList<Manager> managers, ArrayList<Staff> staffs,
//			ArrayList<Computer> computers) {
//		super();
//		this.name = name;
//		this.address = address;
//		this.managers = managers;
//		this.staffs = staffs;
//		this.computers = computers;
//
//	}

	public void addComputer(Computer computer) {
		computers.add(computer);
		totalReport.setNumComputerIn(totalReport.getNumComputerIn() + 1);
	}

	public void deleteComputer(String id) {
		for (int i = 0; i < computers.size(); i++) {

			if (computers.get(i).getId() == id) {
				System.out.println("Computer ID : " + computers.get(i).getId() + " have been removed.");
				computers.remove(i);
				totalReport.setNumComputerOut(totalReport.getNumComputerOut() + 1);
				break;
			}

		}
	}
	
	public void viewReport() {
		System.out.println("Shop name : " + this.name);
		System.out.println("Shop address : " + this.address);
		System.out.println("Number of computer in : " + this.totalReport.getNumComputerIn());
		System.out.println("Number of computer out : " + this.totalReport.getNumComputerOut());
		
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ArrayList<Manager> getManagers() {
		return managers;
	}

	public void setManagers(ArrayList<Manager> managers) {
		this.managers = managers;
	}

	public ArrayList<Staff> getStaffs() {
		return staffs;
	}

	public void setStaffs(ArrayList<Staff> staffs) {
		this.staffs = staffs;
	}

	public ArrayList<Computer> getComputers() {
		return computers;
	}

	public void setComputers(ArrayList<Computer> computers) {
		this.computers = computers;
	}

}
