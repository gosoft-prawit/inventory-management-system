package th.co.gosoft.ims.inventory;

import th.co.gosoft.ims.shop.Shop;

public class Computer {

	private String id;
	private String brand;
	private String cpu;
	private String ram;
	private String harddisk;

	public Computer(String id, String brand, String cpu, String ram, String harddisk) {
		super();
		this.id = id;
		this.brand = brand;
		this.cpu = cpu;
		this.ram = ram;
		this.harddisk = harddisk;
	}

	public static void main(String[] args) {

		Shop shop1 = new Shop();
		Computer acer1 = new Computer("1","Acer", "2Ghz", "8MB", "250GB");
		Computer acer2 = new Computer("2","Acer", "2Ghz", "16MB", "500GB");
//		acer1.addComputerNoStatic(shop1);
//		acer2.addComputerNoStatic(shop1);
		
		Shop shop2 = new Shop();
		shop2.setName("The Tara");
		shop2.setAddress("Chang Wattana");
		System.out.println(shop2.getComputers().size());
		Computer acer3 = new Computer("3","Acer", "2Ghz", "8MB", "250GB");
		Computer acer4 = new Computer("4", "Acer", "2Ghz", "16MB", "500GB");
		shop2.addComputer(acer3);
		shop2.addComputer(acer4);
		System.out.println(shop2.getComputers().size());
		System.out.println("Befor delete");
		shop2.viewReport();
		shop2.deleteComputer("3");
		System.out.println("After delete");
		shop2.viewReport();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getCpu() {
		return cpu;
	}

	public void setCpu(String cpu) {
		this.cpu = cpu;
	}

	public String getRam() {
		return ram;
	}

	public void setRam(String ram) {
		this.ram = ram;
	}

	public String getHarddisk() {
		return harddisk;
	}

	public void setHarddisk(String harddisk) {
		this.harddisk = harddisk;
	}

	public void addComputer() {

	}

	public void deleteComputer() {

	}

	public void editComputer() {

	}

}
