package th.co.gosoft.ims.report;

public class Report {

	private int numComputerIn;
	private int numComputerOut;
	private int numBarcodeScannerIn;
	private int numBarcodeScannerOut;
	private int numEDCIn;
	private int numEDCOut;

	public void viewReport() {

	}

	public int getNumComputerIn() {
		return numComputerIn;
	}

	public void setNumComputerIn(int numComputerIn) {
		this.numComputerIn = numComputerIn;
	}

	public int getNumComputerOut() {
		return numComputerOut;
	}

	public void setNumComputerOut(int numComputerOut) {
		this.numComputerOut = numComputerOut;
	}

	public int getNumBarcodeScannerIn() {
		return numBarcodeScannerIn;
	}

	public void setNumBarcodeScannerIn(int numBarcodeScannerIn) {
		this.numBarcodeScannerIn = numBarcodeScannerIn;
	}

	public int getNumBarcodeScannerOut() {
		return numBarcodeScannerOut;
	}

	public void setNumBarcodeScannerOut(int numBarcodeScannerOut) {
		this.numBarcodeScannerOut = numBarcodeScannerOut;
	}

	public int getNumEDCIn() {
		return numEDCIn;
	}

	public void setNumEDCIn(int numEDCIn) {
		this.numEDCIn = numEDCIn;
	}

	public int getNumEDCOut() {
		return numEDCOut;
	}

	public void setNumEDCOut(int numEDCOut) {
		this.numEDCOut = numEDCOut;
	}

}
