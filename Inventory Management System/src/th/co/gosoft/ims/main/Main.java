package th.co.gosoft.ims.main;

import th.co.gosoft.ims.inventory.Computer;

public class Main {
	public static void main(String[] args) {
		Computer acer1 = new Computer("1", "acer", "2GHz", "16GB", "3TB");
		System.out.println("before " + acer1.getHarddisk());
		acer1.setHarddisk("4TB");
		System.out.println("after " + acer1.getHarddisk());
	}

}
